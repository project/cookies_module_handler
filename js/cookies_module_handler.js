/**
 * @file
 * Attaches the behaviors for the COOKiES Module Handler module.
 */
(function(Drupal, drupalSettings) {
  "use strict";

  Drupal.behaviors.cookiesModuleHandler = {

    activate: function (config, context) {
      this.fallbackUndo(config, context);
      document.querySelectorAll('script[data-cookies-js-id='+config.id+']')
        .forEach(function(script) {
          let content = script.innerHTML;
          let newScript = document.createElement('script');
          let attributes = Array.from(script.attributes);
          for (let attr in attributes) {
            let name = attributes[attr].nodeName;
            if (name !== 'type' && name !== 'data-cookies-js-id') {
              newScript.setAttribute(name, attributes[attr].nodeValue);
            }
          }
          newScript.innerHTML = content;
          script.parentNode.replaceChild(newScript, script);
        })
    },

    fallback: function (config, context) {
      if (typeof config['fallback_action'] === 'undefined' || typeof config['fallback_selector'] === 'undefined')
        return;
      switch(config['fallback_action']) {
        case 'overlay':
          jQuery(config['fallback_selector'], context).cookiesOverlay(config.service);
          break;
        case 'hide':
          context.querySelectorAll(config['fallback_selector']).forEach(function(element) {
            element.classList.add('visually-hidden');
          });
          break;
        case 'remove':
          context.querySelectorAll(config['fallback_selector']).forEach(function(element) {
            element.remove();
          });
          break;
        case 'add_class':
          if (typeof config['fallback_class'] !== 'undefined' && config['fallback_class']) {
            context.querySelectorAll(config['fallback_selector']).forEach(function(element) {
              element.classList.add(config['fallback_class']);
            });
          }
          break;
        case 'rm_class':
          if (typeof config['fallback_class'] !== 'undefined' && config['fallback_class']) {
            context.querySelectorAll(config['fallback_selector']).forEach(function(element) {
              element.classList.remove(config['fallback_class']);
            });
          }
          break;
        default:
      }
    },


    fallbackUndo: function (config, context) {
      if (typeof config['fallback_action'] === 'undefined' || typeof config['fallback_selector'] === 'undefined')
        return;
      switch(config['fallback_action']) {
        case 'hide':
          context.querySelectorAll(config['fallback_selector']).forEach(function(element) {
            element.classList.remove('visually-hidden');
          });
          break;
        case 'add_class':
          if (typeof config['fallback_class'] !== 'undefined' && config['fallback_class']) {
            context.querySelectorAll(config['fallback_selector']).forEach(function(element) {
              element.classList.remove(config['fallback_class']);
            });
          }
          break;
        case 'rm_class':
          if (typeof config['fallback_class'] !== 'undefined' && config['fallback_class']) {
            context.querySelectorAll(config['fallback_selector']).forEach(function(element) {
              element.classList.add(config['fallback_class']);
            });
          }
          break;
        default:
      }
    },

    attach: function (context) {
      let self = this;
      let cookiesJsConfigs = drupalSettings.cookiesModuleHandler;
      document.addEventListener('cookiesjsrUserConsent', function (event) {
        let services = (typeof event.detail.services === 'object') ? event.detail.services : {};
        Object.keys(cookiesJsConfigs).forEach(function(configId, index) {
          let config = cookiesJsConfigs[configId];
          if (typeof services[config.service] !== 'undefined' && services[config.service]) {
            self.activate(config, context);
          } else {
            self.fallback(config, context);
          }
        })

      });
    }
  }
})(Drupal, drupalSettings);
