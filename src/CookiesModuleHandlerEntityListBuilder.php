<?php

namespace Drupal\cookies_module_handler;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of COOKiES Module Handler entity entities.
 */
class CookiesModuleHandlerEntityListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('COOKiES Module Handler');
    $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $description = [
      'description' => ['#markup' => $this->t('<p>COOKiES module handlers are used to modify the HTML output (especially &lt;script&gt; tags) of a module in the backend so that the execution of Javascript and thus the installation of cookies or the transmission of user data to third parties is suppressed until the User of the website has given his consent. If the user has given their consent, the changes in the front end are reversed so that the Javascript code is loaded and executed.</p>
        <p>There are two common methods in Drupal that a module can add Javascript to the HTML of the page. On the one hand the well-known method via libraries, and on the other hand the technique of adding a script tag directly in the header of the HTML page (which is often used by tracking services).</p>
        <p>If any module uses one of these methods to integrate critical Javascript code (within the meaning of the GDPR), a COOKiES module handler can be used to effectively switch off this code until the user has given his consent via the COOKiES UI without this individual adjustments must be programmed for this.)</p>
    ')]];
    $default = parent::render();
    return array_merge($description, $default);
  }
}
