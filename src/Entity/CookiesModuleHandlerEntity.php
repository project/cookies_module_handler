<?php

namespace Drupal\cookies_module_handler\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the COOKiES Module Handler entity entity.
 *
 * @ConfigEntityType(
 *   id = "cookies_module_handler_entity",
 *   label = @Translation("COOKiES Module Handler"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\cookies_module_handler\CookiesModuleHandlerEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\cookies_module_handler\Form\CookiesModuleHandlerEntityForm",
 *       "edit" = "Drupal\cookies_module_handler\Form\CookiesModuleHandlerEntityForm",
 *       "delete" = "Drupal\cookies_module_handler\Form\CookiesModuleHandlerEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\cookies_module_handler\CookiesModuleHandlerEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cookies_module_handler_entity",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "status",
 *     "service",
 *     "module",
 *     "jsfiles",
 *     "page_attachment_id",
 *     "fallback_action",
 *     "fallback_selector",
 *     "fallback_class"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/cookies/module_handler/{cookies_module_handler_entity}",
 *     "add-form" = "/admin/config/cookies/module_handler/add",
 *     "edit-form" = "/admin/config/cookies/module_handler/{cookies_module_handler_entity}/edit",
 *     "delete-form" = "/admin/config/cookies/module_handler/{cookies_module_handler_entity}/delete",
 *     "collection" = "/admin/config/cookies/module_handler"
 *   }
 * )
 */
class CookiesModuleHandlerEntity extends ConfigEntityBase implements CookiesModuleHandlerEntityInterface {

  /**
   * The COOKiES Module Handler entity ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The COOKiES Module Handler entity label.
   *
   * @var string
   */
  protected $label;

  /**
   * The COOKiES service to interact with.
   *
   * @var string
   */
  protected $service;

  /**
   * The module to support with COOKiES consent management.
   *
   * @var string
   */
  protected $module;

  /**
   * Library to support with COOKiES consent management.
   *
   * @var string
   */
  protected $library;

  /**
   * Javascript files to support with COOKiES consent management.
   *
   * @var array
   */
  protected $jsfiles = [];

  /**
   * Page attachment id for javascript added by page attachment to the html_head.
   *
   * @var string
   */
  protected $pageAttachmentId;

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();
    $this->addDependency('module','cookies');
    $this->addDependency('enforced',['module' => ['cookies_module_handler']]);
    $this->addDependency('module', $this->get('module'));
    $service = $this->get('service');
    $service = $this->entityTypeManager()
      ->getStorage('cookies_service')->load($service);
    $this->addDependency('config', $service->getConfigDependencyName());
    return $this;
  }
}
