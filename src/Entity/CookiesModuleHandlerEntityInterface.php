<?php

namespace Drupal\cookies_module_handler\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining COOKiES Module Handler entity entities.
 */
interface CookiesModuleHandlerEntityInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
