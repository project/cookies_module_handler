<?php

namespace Drupal\cookies_module_handler\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CookiesModuleHandlerConfigForm.
 */
class CookiesModuleHandlerConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cookies_module_handler.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cookies_module_handler_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cookies_module_handler.configuration');


    $form['discovery'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Discover page attachments'),
      '#weight' => 10,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['discovery']['enable_page_attachment_discovery'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable page attachment discovery'),
      '#description' => $this->t('Developer option to discover page attachments
         in the html_head.
         <br>If enabled on every page a message appears with all ids of
         page_attachments binding a javascript to the html_head.
         <br>This is useful when you create/debug a COOKiES handler for modules
         using <code>hook_page_attachments()</code> to bind javascript to the very
         top of the HTML head.
         <br><b>Important:</b> Must be disabled in production sites.'),
      '#default_value' => $config->get('enable_page_attachment_discovery'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('cookies_module_handler.configuration')
      ->set('enable_page_attachment_discovery', $form_state->getValue('enable_page_attachment_discovery'))
      ->save();
  }

}
