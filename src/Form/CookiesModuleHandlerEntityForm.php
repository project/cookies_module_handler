<?php

namespace Drupal\cookies_module_handler\Form;

use Drupal\cookies_module_handler\LibrariesService;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CookiesModuleHandlerEntityForm.
 */
class CookiesModuleHandlerEntityForm extends EntityForm {

  /**
   * The famous Drupal Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The famous Drupal Entity Type Manager.
   *
   * @var \Drupal\cookies_module_handler\LibrariesService
   */
  protected $librariesService;


  /**
   * Class constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LibrariesService $libraries_service) {
    $this->entityTypeManager = $entity_type_manager;
    $this->librariesService = $libraries_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('entity_type.manager'),
      $container->get('cookies_module_handler.libraries')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $entity = $this->entity;
    $cookies_module_handler_entity = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $cookies_module_handler_entity->label(),
      '#description' => $this->t("Label for the COOKiES Module Handler entity."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $cookies_module_handler_entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\cookies_module_handler\Entity\CookiesModuleHandlerEntity::load',
      ],
      '#disabled' => !$cookies_module_handler_entity->isNew(),
    ];


    $form['basic'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Basic'),
      '#weight' => 10,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $services = $this->entityTypeManager->getStorage('cookies_service')->loadMultiple();
    $service_options = [];
    foreach ($services as $id => $service) {
      $service_options[$id] = $service->label();
    }
    $form['basic']['service'] = [
      '#type' => 'select',
      '#title' => $this->t('COOKiES Service'),
      '#options' => $service_options,
      '#default_value' => $entity->get('service'),
      '#description' => $this->t("Select a
         <a href='@service_list' target='_blank'>COOKiES service</a> that will
         control this module handler in the front end. You may want to
         <a href='@service_add' target='_blank'>create a new COOKiES service</a>
         first.", [
          '@service_list' => Url::fromRoute('entity.cookies_service.collection')->toString(),
          '@service_add' => Url::fromRoute('entity.cookies_service.add_form')->toString()
        ]),
      '#required' => TRUE,
    ];


    $form['entity'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Javascript files from libraries'),
      '#open' => TRUE,
      '#weight' => 20,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];
    $form['entity']['description'] = [
      '#type' => 'container',
      '#markup' => $this->t("<p>Select javascript files that should be
        protected, while the user has not actively given his consent by COOKiES
        consent management. <br/>
        <b>WARNING:</b> This may cause Javascript errors especially when the
        protected javascript is required by another one.</p>"),
    ];
    $form['entity']['module'] = [
      '#type' => 'select',
      '#title' => $this->t('Module'),
      '#options' => $this->librariesService->getModuleOptionsList(),
      '#default_value' => $entity->get('module'),
      '#description' => $this->t("Select the module including the library, you want to support with COOKiES consent management."),
      '#required' => false,
      '#empty_option' => $this->t('- Select a module -'),
      '#empty_value' => '',
      '#ajax'         => [
        'callback'  => [$this, 'libraryCallback'],
        'event' => 'change',
        'wrapper' => 'jsfiles-wrapper',
        'method' => 'replace',
      ],
    ];

    if ($module = $form_state->getValue('module', $entity->get('module'))) {
      $jsfiles = $this->librariesService->getLibrariesOptionsListByExtension($module);
      $form['entity']['jsfiles'] = [
        '#prefix' => '<div id="jsfiles-wrapper">',
        '#suffix' => '</div>',
        '#type' => 'select',
        '#title' => $this->t('Javascript files'),
        '#options' => $jsfiles,
        '#multiple' => count($jsfiles) >= 1,
        '#disabled' => !count($jsfiles),
        '#size'=> count($jsfiles, COUNT_RECURSIVE),
        '#default_value' => $entity->get('jsfiles'),
        '#empty_option' => !count($jsfiles)
          ? $this->t('No javascript files found')
          : $this->t('- Select javascript files -'),
        '#empty_value' => '',
        '#required' => false,
        '#description' => $this->t("Select files to protect with COOKiES
          consent management. Hold 'CMD' or 'CTRL' button pressed to select
          multiple."),
      ];
    }
    else {
      $form['entity']['jsfiles'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'jsfiles-wrapper'],
      ];
    }

    $form['attachments'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Javascript included by Page attachments'),
      '#weight' => 30,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['attachments']['intro'] = [
      '#type' => 'container',
      'message' => [
        '#markup' => $this->t('<p>Enter a page attachment ID. The page
          attachment ID is usually hidden in the code. You can activate
          the option "Enable page attachment discovery" in the
          <a href="@uri">configuration of the COOKiES Module Handler</a> in
          order to make it visible in a message each time it is called.</p>', [
            '@uri' => Url::fromRoute('cookies_module_handler.config')->toString(),
          ]
        )
      ],
    ];

    $form['attachments']['page_attachment_id'] = [
      '#type' => 'machine_name',
      '#title' => t('Page attachment ID'),
      '#default_value' => $entity->get('page_attachment_id'),
      '#size' => 60,
      '#required' => false,
      '#machine_name' => ['exists' => [$this, 'pageAttachmentsId']],
    ];

    $form['attachments']['outro'] = [
      '#type' => 'container',
      'message' => [
        '#markup' => $this->t('<p><b>INFO:</b> All scripts whose ID <u>begins
           with</u> the entered string are protected by COOKiES Module Handler.<br>
           E.g. if you enter <code>doozle_tag_manager</code> here, the scripts
           with the ID <code>doozle_tag_manager</code> and also
           <code>doozle_tag_manager_analytics, doozle_tag_manager_map</code>
           would be protected.</p>')
      ],
    ];


    $form['fallback'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Fallback actions'),
      '#weight' => 40,
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $form['fallback']['intro'] = [
      '#type' => 'container',
      'message' => [
        '#markup' => $this->t('<p>Fallback to execute when the COOKiES service has (already) disabled a 3rd party plugin. The fallback action is only intended for cosmetic repairs to the layout.<br/>
            <strong>IMPORTANT:</strong> With the fallback action, neither the loading of an iFrame nor the execution of Javascript can be effectively prevented.</p>'
        )
      ],
    ];
    $form['fallback']['fallback_action'] = [
      '#type' => 'select',
      '#title' => $this->t('Fallback action'),
      '#options' => [
        'overlay' => 'Add overlay on selector (as sibling)',
        'hide' => 'Set selector hidden',
        'remove' => 'Remove selector from DOM',
        'add_class' => 'Add class to selector',
        'rm_class' => 'Remove class from selector',
      ],
      '#default_value' => $entity->get('fallback_action'),
      '#empty_option' => $this->t('- Select fallback action -'),
      '#empty_value' => '',
      '#required' => false,
      '#description' => $this->t("How to deal with the gap that arises from the fact that e.g. a 3rd party widget is not displayed."),
      '#ajax' => [
        'callback'  => [$this, 'fallbackActionCallback'],
        'event' => 'change',
        'wrapper' => 'fallback-class-wrapper',
        'method' => 'replace',
      ],
    ];

    $form['fallback']['fallback_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fallback selector'),
      '#default_value' => $entity->get('fallback_selector'),
      '#required' => false,
      '#description' => $this->t("Enter css selector, e.g. '.field iframe.gmap'."),
    ];

    $fallback_action = $entity->get('fallback_action');
    if (in_array($fallback_action, ['add_class', 'rm_class'])) {
      $form['fallback']['fallback_class'] = [
        '#prefix' => '<div id="fallback-class-wrapper">',
        '#suffix' => '</div>',
        '#type' => 'textfield',
        '#title' => $this->t('Class to add/remove'),
        '#default_value' => $entity->get('fallback_class'),
        '#required' => true,
        '#description' => $this->t("Enter class name (without leading dot)."),
      ];
    }
    else {
      $form['fallback']['fallback_class'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'fallback-class-wrapper'],
      ];
    }
    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  public function pageAttachmentsId($id) {
    // @ToDo Check if another config entity already use same page attachment.
    return false;
  }


  /**
   * Ajax Callback when library has changed.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   Returns a renderable part of the form for an ajax call.
   */
  public function moduleCallback(array $form, FormStateInterface $form_state) {
    return $form['entity']['library'];
  }

  /**
   * Ajax Callback when library has changed.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   Returns a renderable part of the form for an ajax call.
   */
  public function libraryCallback(array $form, FormStateInterface $form_state) {
    return $form['entity']['jsfiles'];
  }

  /**
   * Ajax Callback when fallback_action has changed.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   Returns a renderable part of the form for an ajax call.
   */
  public function fallbackActionCallback(array $form, FormStateInterface $form_state) {
    return $form['fallback']['fallback_class'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $cookies_module_handler_entity = $this->entity;

    // Prepare jsfiles for saving in config by transforming asoc to indic array.
    if ($jsfiles = $cookies_module_handler_entity->get('jsfiles')) {
      $jsfiles_save = [];
      foreach ($jsfiles as $jsfile_key => $jsfile_value) {
        if ($jsfile_value) {
          $jsfiles_save[] = $jsfile_key;
        }
      }
      $cookies_module_handler_entity->set('jsfiles', $jsfiles_save);
    }
    $status = $cookies_module_handler_entity->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label COOKiES Module Handler entity.', [
          '%label' => $cookies_module_handler_entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label COOKiES Module Handler entity.', [
          '%label' => $cookies_module_handler_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($cookies_module_handler_entity->toUrl('collection'));
  }

}
