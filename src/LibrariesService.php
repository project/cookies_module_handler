<?php

namespace Drupal\cookies_module_handler;
use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class LibrariesService.
 */
class LibrariesService {
  use StringTranslationTrait;

  /**
   * Drupal\Core\Asset\LibraryDiscoveryInterface definition.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Drupal\Core\Entity\EntityTypeManager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Extension\ModuleExtensionList definition.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleHandler;

  /**
   * Data collection to handle knock-out and healing of library files.
   *
   * @var array
   */
  private $activeConfig;

  /**
   * Constructs a new LibrariesService object.
   *
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Extension\ModuleHandler $module_handler
   */
  public function __construct(
    LibraryDiscoveryInterface $library_discovery,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandler $module_handler
  ) {
    $this->libraryDiscovery = $library_discovery;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Load filtered config entities of type cookies_module_handler_entity;
   *
   * @param boolean $assoc
   *   If true the method return an array of assoc arrays instead of objects.
   *
   * @return array|\Drupal\cookies_module_handler\Entity\CookiesModuleHandlerEntity[]
   *   Collection of CookiesModuleHandlerEntity
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCookiesModuleHandlerEntities(bool $assoc = false) {
    /** @var \Drupal\cookies_module_handler\Entity\CookiesModuleHandlerEntity[] $entities */
    if(!$this->activeConfig) {
      $this->activeConfig = $this->entityTypeManager
        ->getStorage('cookies_module_handler_entity')
        ->loadByProperties(['status' => 1]);
    }
    if ($assoc) {
      $collector = [];
      foreach($this->activeConfig as $config_id => $entity) {
        $collector[$config_id] = $entity->toArray();
        unset($collector[$config_id]['uuid']);
      }
      return $collector;
    }
    return $this->activeConfig;
  }

  /**
   * Return an option array for select module.
   *
   * @return array
   *   Return an option array for select module.
   */
  public function getModuleOptionsList() {
    $module_options = [];
    foreach ($this->moduleHandler->getModuleList() as $name => $extension) {
      $module_options[$name] = $this->moduleHandler->getName($name);
    }
    return $module_options;
  }

  /**
   * @param string $extension
   *
   * @return array
   */
  public function getLibrariesOptionsListByExtension(string $extension) {
    $libraries_opts = [];
    foreach($this->libraryDiscovery->getLibrariesByExtension($extension) as $key => $library) {
      $files = $this->getJsFilesByLibrary($extension, $key);
      if (!empty($files)) {
        $libraries_opts['Library: '.$key] = $files;
      }
    };
    return $libraries_opts;
  }

  public function getJsFilesByLibrary(string $module, string $library_id) {
    $files = [];
    if ($library = $this->libraryDiscovery->getLibraryByName($module, $library_id)) {
      if ($library['js']) {
        foreach ($library['js'] as $libraryJs) {
          if (in_array($libraryJs['type'], ['file', 'external'])) {
            $files[$libraryJs['data']] = $libraryJs['data'];
          }
        }
      }
    }
    return $files;
  }
}
